from mesa import Agent
import math
from random import randint


class Wall(Agent):
    def __init__(self, id, model):
        self.unique_id = id
        self.model = model

    @staticmethod
    def Build_BotSxRoom(model):
        column  = math.floor(model.grid.width / 2) - math.floor(model.grid.width / 10)
        row     = math.floor(model.grid.height / 2) - math.floor(model.grid.height / 10)

        door_1 = randint(0, column - 1)
        door_2 = randint(0, row - 1)
        for i in range(column + 1):
            if not i == door_1:
                w = Wall(i, model)
                w.add(row, i)
        for i in range(row):
            if not i == door_2:
                w = Wall(i, model)
                w.add(i, column)

    @staticmethod
    def Build_BotDxRoom(model):
        column  = model.grid.width - math.floor(model.grid.width / 2) + math.floor(model.grid.width / 10)
        row     = math.floor(model.grid.height / 2) - math.floor(model.grid.height / 10)

        door_1 = randint(column, model.grid.width)
        door_2 = randint(0, row - 1)
        for i in range(row):
            if not i == door_2:
                w = Wall(i, model)
                w.add(column, i)

        for i in range(column, model.grid.width):
            if not i == door_1:
                w = Wall(i, model)
                w.add(i, row)

    @staticmethod
    def Build_UpSxRoom(model):
        column  = math.floor(model.grid.width / 2) - math.floor(model.grid.width / 10)
        row     = model.grid.height - math.floor(model.grid.height / 2) + math.floor(model.grid.height / 10)

        door_1 = randint(0, column)
        door_2 = randint(row, model.grid.height)
        for i in range(row, model.grid.height):
            if not i == door_2:
                w = Wall(i, model)
                w.add(column, i)

        for i in range(column):
            if not i == door_1:
                w = Wall(i, model)
                w.add(i, row)

    @staticmethod
    def Build_UpDxRoom(model):
        column  = model.grid.width - math.floor(model.grid.width / 2) + math.floor(model.grid.width / 10)
        row     = model.grid.height - math.floor(model.grid.height / 2) + math.floor(model.grid.height / 10)

        door_1 = randint(column, model.grid.width)
        door_2 = randint(row, model.grid.height)
        for i in range(row, model.grid.height):
            if not i == door_2:
                w = Wall(i, model)
                w.add(column, i)

        for i in range(column, model.grid.width):
            if not i == door_1:
                w = Wall(i, model)
                w.add(i, row)

    def remove(self):
        self.model.grid._remove_agent(self.pos, self)  # rimuovo l'agent Susceptible dalla griglia
        self.model.schedule.remove(self)

    def add(self, row, column):
        self.model.schedule.add(self)
        self.model.grid.place_agent(self, (row, column))


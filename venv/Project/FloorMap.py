from Project.Room import Room
from Project.Wall import Wall
from Project.ExitAgent import Exit
from Project.EntryAgent import Entry
from Project.navigator import Pathfinder
import json


class FloorMap:
    def __init__(
                 self,
                 file_path,
                 model
            ):
        with open(file_path) as file:
            result = json.load(file)
            result = result['grid']

        self.model = model
        self.entrances = self.get_tuples(result['floor']['entrances'])
        self.exits = self.get_tuples(result['floor']['exits'])
        self.rooms = []

        """build all the rooms from JSON"""
        for room in result['floor']['rooms']:
            self.rooms.append(Room(room, self.model))

        self.build_margins(self.model)

        """remove Walls to place entrances"""
        for entry in self.entrances:
            if not self.model.grid.is_cell_empty(entry):
                a = self.model.schedule.get_agent_by_pos(entry)
                a.remove()

        self.entrances = self.create_entrances(self.entrances)

        """remove Walls to place exits"""
        for exit in self.exits:
            if not self.model.grid.is_cell_empty(exit):
                a = self.model.schedule.get_agent_by_pos(exit)
                a.remove()

        self.exits = self.create_exits(self.exits)

        """ apply formal control """
        """invalid_rooms_entry = self.formal_control()
        if len(invalid_rooms_entry) > 0:
            for room in invalid_rooms_entry:
                print(f"Stanza {room.id} non raggiungibile!")"""

    def build_margins(self, model):
        width   = model.grid.width - 1
        height  = model.grid.height - 1

        for i in range(0, width + 1):
            if self.model.grid.is_cell_empty((0, i)):
                w1 = Wall(self.model.schedule.next_id(breed=Wall), model)
                w1.add(0, i)

            if self.model.grid.is_cell_empty((height, i)):
                w2 = Wall(self.model.schedule.next_id(breed=Wall), model)
                w2.add(height, i)

        for i in range(0, height + 1):
            if self.model.grid.is_cell_empty((i, 0)):
                w1 = Wall(self.model.schedule.next_id(breed=Wall), model)
                w1.add(i, 0)
            if self.model.grid.is_cell_empty((i, width)):
                w2 = Wall(self.model.schedule.next_id(breed=Wall), model)
                w2.add(i, width)

    def pairwise(self, list):
        a = iter(list)
        return zip(a, a)

    def get_tuples(self, list):
        tuples = []
        for x, y in self.pairwise(list):
            tuples.append((x, y))

        return tuples

    def get_tuple(self, list):
        return (list[0], list[1])

    def get_room_by_id(self, id):
        return [room for room in self.rooms if room.id == id]

    def get_rooms_entrances(self):
        list = []
        for room in self.rooms:
            list.append(room.entry.position)

        return list

    def get_rooms_exits(self):
        list = []
        for room in self.rooms:
            list.append(room.exit.position)

        return list

    def create_entrances(self, entrances):
        list = []
        for entry in entrances:
            e = Entry(self.model.schedule.next_id(), entry, self.model)
            e.add()
            list.append(e)

        return list

    def create_exits(self, eixts):
        list = []
        for exit in eixts:
            e = Exit(self.model.schedule.next_id(), exit, self.model)
            e.add()
            list.append(e)

        return list

    def formal_control(self):
        """ for each room on the floor, check if exists a path from any entry to room center,    """
        """ then, if exists, check if exists a path from room center to any exit    """
        invalid_room_entry = self.rooms[:]  # [:] copio la lista per valore, e non per riferimenti
        for room in self.rooms:
            for entry in self.entrances:
                path = Pathfinder(self.model).find_shortest_path(entry.position, room.center)
                if path and room in invalid_room_entry:
                    invalid_room_entry.remove(room)

        return invalid_room_entry

    def is_agent_any_room(self, agent_pos):
        for room in self.rooms:
            for cell in room.cells:
                if agent_pos == cell:
                    return True
        return False

    def saturate_rooms_air(self):
        for room in self.rooms:
            agents = room.get_agents_inside_room()
            if len(agents) > 0:
                room.air_saturation += len(agents)
                room.update_agents_infectability_from_air_saturation(agents)






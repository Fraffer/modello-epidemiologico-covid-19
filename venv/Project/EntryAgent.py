from mesa import Agent


class Entry(Agent):
    def __init__(self, id, position, model):
        self.unique_id = id
        self.position = position
        self.model = model

    def add(self):
        self.model.schedule.add(self)
        self.model.grid.place_agent(self, self.position)
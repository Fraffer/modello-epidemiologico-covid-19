from Project.Wall import Wall
import json
from Project.ExitAgent import Exit
from Project.EntryAgent import Entry

class Room:
    def __init__(self, json_str, model):
        self.__dict__ = json_str['room']
        self.model = model
        self.center = self.get_tuple(self.center)
        self.entry = self.get_tuple(self.entry)
        self.exit = self.get_tuple(self.exit)
        self.bot_sx = self.get_tuple(self.bot_sx)
        self.up_dx = self.get_tuple(self.up_dx)
        self.air_saturation = 0
        self.agents = []
        self.cells = []

        e1 = Entry(self.model.schedule.next_id(), self.entry, self.model)
        e1.add()
        self.entry = e1
        e2 = Exit(self.model.schedule.next_id(), self.exit, self.model)
        e2.add()
        self.exit = e2

        self.build()

    def build(self):
        row_bot, col_left = self.bot_sx
        row_up, col_rigth = self.up_dx

        """ add all the cells contained in the room in the appropriate list """
        for i in range(row_bot, row_up + 1):
            for j in range(col_left, col_rigth + 1):
                self.cells.append((i, j))

        """ augment by one all the vertices to start build room's walls """
        row_bot     -= 1
        row_up      += 1
        col_left    -= 1
        col_rigth   += 1

        for i in range(col_left, col_rigth + 1):
            if self.model.grid.is_cell_empty((row_bot, i)):
                a1 = Wall(self.model.schedule.next_id(breed=Wall), self.model)
                a1.add(row_bot, i)

            if self.model.grid.is_cell_empty((row_up, i)):
                a2 = Wall(self.model.schedule.next_id(breed=Wall), self.model)
                a2.add(row_up, i)

        for i in range(row_bot, row_up + 1):
            if self.model.grid.is_cell_empty((i, col_left)):
                a1 = Wall(self.model.schedule.next_id(breed=Wall), self.model)
                a1.add(i, col_left)

            if self.model.grid.is_cell_empty((i, col_rigth)):
                a2 = Wall(self.model.schedule.next_id(breed=Wall), self.model)
                a2.add(i, col_rigth)

    def pairwise(self, list):
        a = iter(list)
        return zip(a, a)

    def get_tuples(self, list):
        tuples = []
        for x, y in self.pairwise(list):
            tuples.append((x, y))

        return tuples

    def get_tuple(self, list):
        return (list[0], list[1])

    def update_current_agents(self):
        self.agents = self.get_agents_inside_room()

    def get_agents_inside_room(self):
        list = []
        for cell in self.cells:
            cell_content = self.model.grid.get_cell_list_contents(cell)
            for agent in cell_content:
                list.append(agent.unique_id)
        return list

    def update_agents_infectability_from_air_saturation(self, new_agents):
        """increase agents infectability if room's air is saturated till they are inside"""

        """if not already done, update infectability of all agents inside room"""
        if self.air_saturation > len(self.cells):
            for agent_id in self.agents:
                agent = self.model.schedule.get_agent_by_id(agent_id)
                if agent.infectability == agent.get_infectability():
                    #print(f"{type(agent).__name__} n. {agent.unique_id} in room {self.id} - infectability go from {agent.infectability}")
                    agent.infectability *= 1 + self.model.prob_be_infected_no_recirculation / 100
                    #print(f"to {agent.infectability}! ")

            for agent_id in self.agents:
                """if an agent is not inside new list, then it exits the room, so i have to bring back his orignal infectability"""
                if agent_id not in new_agents and agent.infectability > agent.get_infectability():
                    agent = self.model.schedule.get_agent_by_id(agent_id)
                    #print(f"{type(agent).__name__} n. {agent.unique_id} exist {self.id} - infectability go from {agent.infectability}")
                    agent.infectability /= 1 + self.model.prob_be_infected_no_recirculation / 100
                    #print(f"to {agent.infectability}! ")
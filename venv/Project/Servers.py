from mesa.visualization.modules import CanvasGrid, ChartModule, TextElement
from mesa.visualization.UserParam import UserSettableParameter
from Project.Susceptible import Susceptible
from Project.Infected import Infected
from Project.Recovered import Recovered
from Project.Wall import Wall
from Project.Cough import Cough
from Project.navigator import Node
from Project.model_types import types
from Project.EntryAgent import Entry
from Project.ExitAgent import Exit


def open_portrayal(agent):
    portrayal = {"pos": agent.pos}
    if type(agent) is Susceptible:
        portrayal["Shape"] = "circle"
        portrayal["Color"] = "blue"
        portrayal["Filled"] = "true"
        portrayal["r"] = 0.5
        #portrayal["Shape"]      = "res/susceptible.png"
        portrayal["type:"]          = "Susceptible"
        portrayal["Layer"]          = 1
        portrayal["id"]             = agent.unique_id
        portrayal["gender"]         = agent.gender
        portrayal["age"]            = agent.age
        portrayal["ill"]            = agent.ill
        portrayal["mask"]           = agent.wear_mask
        portrayal["gloves"]         = agent.wear_gloves
        portrayal["infectability"]  = agent.infectability
        portrayal["status"]         = agent.status
    elif type(agent) is Infected:
        portrayal["type:"] = "Infected"
        portrayal["Layer"] = 1
        portrayal["id"] = agent.unique_id
        portrayal["gender"] = agent.gender
        portrayal["age"] = agent.age
        portrayal["ill"] = agent.ill
        portrayal["mask"] = agent.wear_mask
        portrayal["gloves"] = agent.wear_gloves
        portrayal["infectability"] = agent.infectability
        portrayal["status"] = agent.status
        portrayal["infection_status"] = agent.infection_status
        if agent.dead:
            portrayal["Shape"] = "res/death.png"
        else:
            portrayal["Shape"] = "circle"
            portrayal["Color"] = "red"
            portrayal["Filled"] = "true"
            portrayal["r"] = 0.7
            #portrayal["Shape"] = "res/infected.png"
    elif type(agent) is Recovered:
        portrayal["type:"] = "Recovered"
        portrayal["Layer"] = 1
        portrayal["id"] = agent.unique_id
        portrayal["gender"] = agent.gender
        portrayal["age"] = agent.age
        portrayal["ill"] = agent.ill
        portrayal["mask"] = agent.wear_mask
        portrayal["gloves"] = agent.wear_gloves
        portrayal["infectability"] = agent.infectability
        portrayal["status"] = agent.status
        portrayal["Shape"] = "circle"
        portrayal["Color"] = "green"
        portrayal["Filled"] = "true"
        portrayal["r"] = 0.9
        #portrayal["Shape"] = "res/recovered.png"
    elif type(agent) is Wall:
        portrayal["Layer"] = 1
        portrayal["Shape"] = "res/wall1.png"
    elif type(agent) is Cough:
        portrayal["Layer"] = 1
        portrayal["Shape"] = "res/cough.png"
    elif type(agent) is Node:
        text = ''
        if agent.parent is not None:
            y_child, x_child = agent.position
            y_parent, x_parent = agent.parent.position
            if y_child > y_parent:
                text = '<'
            elif y_child < y_parent:
                text = '>'
            elif x_child > x_parent:
                text = 'v'
            elif x_child < x_parent:
                text = '^'
        portrayal["Layer"] = 1
        portrayal["id"] = agent.unique_id
        portrayal["parent"] = agent.parent.position if agent.parent is not None else 'START'
        portrayal["position"] = agent.position
        portrayal["Shape"] = "circle"
        portrayal["Filled"] = True
        portrayal["Color"] = "red"
        portrayal["text"] = text
        portrayal["text_color"] = "white"
        portrayal["r"] = 0.9
    elif type(agent) is Exit:
        portrayal["Layer"] = 0
        portrayal["Shape"] = "rect"
        portrayal["Color"] = "rgba(255, 0, 0, 0.3)"
        portrayal["Filled"] = True
        portrayal["w"] = 0.8
        portrayal["h"] = 0.8
        #portrayal["Shape"] = "res/exit.png"
    elif type(agent) is Entry:
        portrayal["Layer"] = 0
        portrayal["Shape"] = "rect"
        portrayal["Color"] = "rgba(0,255,0,0.3)"
        portrayal["Filled"] = True
        portrayal["w"] = 0.8
        portrayal["h"] = 0.8
        #portrayal["Shape"] = "res/entry.png"
    return portrayal


open_grid = CanvasGrid(open_portrayal, 50, 50, 500, 500)


class Counters_openspace(TextElement):
    def render(self, model):
        infected        = str(model.schedule.get_breed_count(Infected))
        susceptible     = str(model.schedule.get_breed_count(Susceptible))
        recovered       = str(model.schedule.get_breed_count(Recovered))
        dead            = str(model.schedule.get_dead_count())
        self_quarantine = str(model.schedule.get_self_quarantine_count())
        quarantine      = str(model.schedule.get_quarantine_count())
        curr_hospitalized   = str(model.schedule.get_curr_hospitalized_agents())
        tot_hospitalized    = str(model.schedule.get_tot_hospitalized_agents())
        curr_intensive_care = str(model.schedule.get_current_intensive_care_agents())
        tot_intensive_care  = str(model.schedule.get_tot_intensive_care_agents())
        man_count   = str(model.schedule.get_man_count())
        woman_count = str(model.schedule.get_woman_count())
        avg_age     = str(model.schedule.get_avg_age())
        day         = str(model.schedule.get_day())
        tot_tampons = str(model.schedule.get_tot_tampons_count())
        positive_tampons = str(model.schedule.get_positive_tampons_count())

        return "<br>Day: {}\
                <br>Infected: {}\
                <br>Susceptible: {}\
                <br>Recovered: {}\
                <br>Dead: {}\
                <br>Total tampons: {}\
                <br>Positive tampons: {}\
                <br>Current self-quarantine: {}\
                <br>Current quarantine: {}\
                <br>Current hospitalized: {}\
                <br>Total hospitalized: {}\
                <br>Current intensive care: {}\
                <br>Total intensive care: {}\
                <br>Man count: {}\
                <br>Woman count: {}\
                <br>Average age: {}\
               ".format(day,
                        infected,
                        susceptible,
                        recovered,
                        dead,
                        tot_tampons,
                        positive_tampons,
                        self_quarantine,
                        quarantine,
                        curr_hospitalized,
                        tot_hospitalized,
                        curr_intensive_care,
                        tot_intensive_care,
                        man_count,
                        woman_count,
                        avg_age)


chart_openspace = ChartModule(
    [
        {"Label": "Infected",           "Color": "#ff0000"},
        {"Label": "Susceptible",        "Color": "#0000ff"},
        {"Label": "Recovered",          "Color": "#20ff00"},
        {"Label": "Dead",               "Color": "#000000"},
        {"Label": "Self-quarantine",    "Color": "#ffff00"},
        {"Label": "Quarantine",         "Color": "#ff66ff"},
        {"Label": "Hospitalized",       "Color": "#ff9600"},
        {"Label": "Intensive_care",     "Color": "#2e7400 "},
    ]
)

model_params_openspace = {
    "model_type": types['open'],
    "infect_again": UserSettableParameter("checkbox", "Recovered can be infected again", False),
    "num_suscetible": UserSettableParameter("slider", "number of susceptible", 1, 1, 500),
    "num_infected": UserSettableParameter("slider", "number of infected", 1, 1, 500),
    "prob_is_man": UserSettableParameter("slider", "probability that enter a man", 50, 0, 100),
    "prob_wear_mask": UserSettableParameter("slider", "probability that enter an agent with mask", 50, 0, 100),
    "prob_wear_gloves": UserSettableParameter("slider", "probability that enter an agent with gloves", 50, 0, 100),
    "prob_is_ill": UserSettableParameter("slider", "probability that enter an agent illed", 50, 0, 100),
    #"prob_is_asyntomatic": UserSettableParameter("slider", "probability that an agent is asyntomatic", 50, 0, 100),
    "prob_infect": UserSettableParameter("slider", "probability Infected transmit infection", 50, 0, 100),
    "prob_be_infected": UserSettableParameter("slider", "probability Susceptible get infected", 50, 0, 100),
    "prob_infect_wearing_mask": UserSettableParameter("slider", "probability Infected transmit infection wearing mask", 50, 0, 100),
    "prob_infect_wearing_gloves": UserSettableParameter("slider", "probability Infected transmit infection wearing gloves", 50, 0, 100),
    "prob_be_infected_wearing_mask": UserSettableParameter("slider", "probability Susceptible get infected wearing mask", 50, 0, 100),
    "prob_be_infected_wearing_gloves": UserSettableParameter("slider", "probability Susceptible get infected wearing gloves", 50, 0, 100),
    "break_quarantine_prob": UserSettableParameter("slider", "probability that an agent break quarantine", 0, 0, 100),
    "quarantine_duration": UserSettableParameter("slider", "quarantine duration", 1, 0, 25),
    "repeat_self_quarantine": UserSettableParameter("checkbox", "agent can go auto-quarantine more times", False),
    "self_quarantine_prob": UserSettableParameter("slider", "probability that an agent goes in auto_quarantine", 0, 0,
                                                  100),
    "self_quarantine_duration": UserSettableParameter("slider", "self-quarantine duration", 1, 0, 25),
    "periodic_tampons": UserSettableParameter("checkbox", "agents are subjected to periodic tampons", False),
    "tampons_rate": UserSettableParameter("slider", "periodic tampons rate", 1, 1, 50),
    "periodic_tampons_prob": UserSettableParameter("slider", "Percentage of agent subjected to periodic tampons", 0, 0,
                                                   100),
    "symptoms_tampons": UserSettableParameter("checkbox", "agents make tampons if show symptoms ", False),
    "contacts_tracking": UserSettableParameter("checkbox", "check previous contact if tampon is positive", False),
    "contacts_tracking_precision": UserSettableParameter("slider", "define conctact tracking precision<br/> "
                                                                   "(0 = no contact tracked, 100 = all last n contact tracked)",
                                                         100, 1, 100),
    "contacts_tracking_capacity": UserSettableParameter("slider", "save last n contact", 1, 1, 50),
    "social_distancing": UserSettableParameter("checkbox", "Pepole mantain social distancing", False),
    "break_social_distancing": UserSettableParameter(
        "slider", "Probability that an agent can break social distancing <br/>"
                  "(0 = impossible, 10 = everyone)", 0, 0, 100)
}


"""                             CLOSE SPACE                              """
def close_portrayal(agent):
    portrayal = {"pos": agent.pos}
    if type(agent) is Susceptible:
        #portrayal["Shape"] = "circle"
        #portrayal["Color"] = "blue"
        #portrayal["Filled"] = "true"
        #portrayal["r"] = 0.5
        portrayal["Shape"]      = "res/susceptible.png"
        portrayal["type:"]          = "Susceptible"
        portrayal["Layer"]          = 1
        portrayal["id"]             = agent.unique_id
        portrayal["gender"]         = agent.gender
        portrayal["age"]            = agent.age
        portrayal["ill"]            = agent.ill
        portrayal["mask"]           = agent.wear_mask
        portrayal["gloves"]         = agent.wear_gloves
        portrayal["infectability"]  = agent.infectability
        portrayal["status"]         = agent.status
    elif type(agent) is Infected:
        portrayal["type:"] = "Infected"
        portrayal["Layer"] = 1
        portrayal["id"] = agent.unique_id
        portrayal["gender"] = agent.gender
        portrayal["age"] = agent.age
        portrayal["ill"] = agent.ill
        portrayal["mask"] = agent.wear_mask
        portrayal["gloves"] = agent.wear_gloves
        portrayal["infectability"] = agent.infectability
        portrayal["status"] = agent.status
        portrayal["infection_status"] = agent.infection_status
        if agent.dead:
            portrayal["Shape"] = "res/death.png"
        else:
            #portrayal["Shape"] = "circle"
            #portrayal["Color"] = "red"
            #portrayal["Filled"] = "true"
            #portrayal["r"] = 0.7
            portrayal["Shape"] = "res/infected.png"
    elif type(agent) is Recovered:
        portrayal["type:"] = "Recovered"
        portrayal["Layer"] = 1
        portrayal["id"] = agent.unique_id
        portrayal["gender"] = agent.gender
        portrayal["age"] = agent.age
        portrayal["ill"] = agent.ill
        portrayal["mask"] = agent.wear_mask
        portrayal["gloves"] = agent.wear_gloves
        portrayal["infectability"] = agent.infectability
        portrayal["status"] = agent.status
        #portrayal["Shape"] = "circle"
        #portrayal["Color"] = "green"
        #portrayal["Filled"] = "true"
        #portrayal["r"] = 0.9
        portrayal["Shape"] = "res/recovered.png"
    elif type(agent) is Wall:
        portrayal["Layer"] = 1
        portrayal["Shape"] = "res/wall1.png"
    elif type(agent) is Cough:
        portrayal["Layer"] = 1
        portrayal["Shape"] = "res/cough.png"
    elif type(agent) is Node:
        text = ''
        if agent.parent is not None:
            y_child, x_child = agent.position
            y_parent, x_parent = agent.parent.position
            if y_child > y_parent:
                text = '<'
            elif y_child < y_parent:
                text = '>'
            elif x_child > x_parent:
                text = 'v'
            elif x_child < x_parent:
                text = '^'
        portrayal["Layer"] = 1
        portrayal["id"] = agent.unique_id
        portrayal["parent"] = agent.parent.position if agent.parent is not None else 'START'
        portrayal["position"] = agent.position
        portrayal["Shape"] = "circle"
        portrayal["Filled"] = True
        portrayal["Color"] = "red"
        portrayal["text"] = text
        portrayal["text_color"] = "white"
        portrayal["r"] = 0.9
    elif type(agent) is Exit:
        portrayal["Layer"] = 0
        portrayal["Shape"] = "rect"
        portrayal["Color"] = "rgba(255, 0, 0, 0.3)"
        portrayal["Filled"] = True
        portrayal["w"] = 0.8
        portrayal["h"] = 0.8
        #portrayal["Shape"] = "res/exit.png"
    elif type(agent) is Entry:
        portrayal["Layer"] = 0
        portrayal["Shape"] = "rect"
        portrayal["Color"] = "rgba(0,255,0,0.3)"
        portrayal["Filled"] = True
        portrayal["w"] = 0.8
        portrayal["h"] = 0.8
        #portrayal["Shape"] = "res/entry.png"
    return portrayal

class Counters_closespace(TextElement):
    def render(self, model):
        infected = str(model.schedule.get_breed_count(Infected))
        susceptible = str(model.schedule.get_breed_count(Susceptible))
        infected_entered = str(model.schedule.get_entered_agents_count(Infected))
        susceptible_entered = str(model.schedule.get_entered_agents_count(Susceptible))
        infected_out = str(model.schedule.get_agents_out_count(Infected))
        susceptible_out = str(model.schedule.get_agents_out_count(Susceptible))
        agents_infected = str(model.schedule.get_agents_infected())
        infection_ratio = str(model.schedule.get_infection_ratio())

        return "<br>Infected: {}\
                <br>Susceptible: {}\
                <br>Infected entered: {}\
                <br>Susceptible entered: {}\
                <br>Infected out: {}\
                <br>Susceptible out: {}\
                <br>Agents infected: {}".format(infected,
                                                susceptible,
                                                infected_entered,
                                                susceptible_entered,
                                                infected_out,
                                                susceptible_out,
                                                agents_infected)


close_grid = CanvasGrid(close_portrayal, 20, 20, 500, 500)


chart_closespace = ChartModule(
    [
        {"Label": "Agents infected",           "Color": "#ff0000"}
    ]
)

model_params_closespace = {
    "model_type":   types['close'],
    "prob_enter_someone": UserSettableParameter("slider", "probability that someone enter", 50, 1, 100),
    "prob_enter_infected": UserSettableParameter("slider", "probability that enter an Infected", 50, 0, 100),
    "prob_wear_mask": UserSettableParameter("slider", "probability that enter an agent with mask", 50, 0, 100),
    "prob_wear_gloves": UserSettableParameter("slider", "probability that enter an agent with gloves", 50, 0, 100),
    "prob_is_ill": UserSettableParameter("slider", "probability that enter an agent illed", 50, 0, 100),
    "prob_infect": UserSettableParameter("slider", "probability Infected transmit infection", 50, 0, 100),
    "prob_be_infected": UserSettableParameter("slider", "probability Susceptible get infected", 50, 0, 100),
    "prob_infect_wearing_mask": UserSettableParameter("slider", "probability Infected transmit infection wearing mask", 50, 0, 100),
    "prob_infect_wearing_gloves": UserSettableParameter("slider", "probability Infected transmit infection wearing gloves", 50, 0, 100),
    "prob_be_infected_wearing_mask": UserSettableParameter("slider", "probability Susceptible get infected wearing mask", 50, 0, 100),
    "prob_be_infected_wearing_gloves": UserSettableParameter("slider", "probability Susceptible get infected wearing gloves", 50, 0, 100),
    "sanitizers": UserSettableParameter("checkbox", "entrances are provided with sanitizers", False),
    "prob_sanitizer_used": UserSettableParameter("slider", "probability that agents use sanitizers", 0, 0, 100),
    "social_distancing": UserSettableParameter("checkbox", "agents mantain social distancing", False),
    "break_social_distancing": UserSettableParameter(
        "slider", "Probability that an agent can break social distancing <br/>"
              "(0 = impossible, 10 = everyone)", 0, 0, 100),
    "air_recirculation": UserSettableParameter("checkbox", "rooms are provided with air recirculation", True),
    "prob_be_infected_no_recirculation": UserSettableParameter("slider", "probability that agents are infected without air recirculation", 0, 0, 100),
}

from Project.PersonAgent import Person
from queue import Queue

class Recovered(Person):
    def __init__(self,
                 id,
                 gender,
                 age,
                 bIll,
                 bWearMask,
                 bWearGloves,
                 status,
                 out_grid_duration,
                 already_self_quarantine,
                 contacts,
                 model):
        super().__init__(id, gender, age, bIll, bWearMask, bWearGloves, model)

        self.status = status
        self.out_grid_duration = out_grid_duration
        self.already_self_quarantine = already_self_quarantine
        self.contacts = contacts
        print(f"Recovered n° {self.unique_id} - gender: {self.gender} - age:{self.age} - ill: {self.ill} - mask: {self.wear_mask} - gloves: {self.wear_gloves} - stato: {self.status}")

    def step(self):
        super().step()

    def move(self):
        super().move()
        cellmates = self.model.grid.get_cell_list_contents([self.pos])
        if len(cellmates) > 1:
            if self in cellmates:
                cellmates.remove(self)
            for agent in cellmates:
            #other = self.random.choice(cellmates)
                if self.model.contacts_tracking:
                    self.add_contact(agent.unique_id)
            # print(f"Contatto tra Infetto n°{self.unique_id} e {type(other).__name__} n° {other.unique_id}")


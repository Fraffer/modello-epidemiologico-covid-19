from mesa import Agent
from random import *

from Project.Susceptible import Susceptible
from Project.Recovered import Recovered
from Project.Infected import Infected

class Cough(Agent):
    def __init__(self, id, infectabilty, model):
        self.unique_id = id
        self.infectability = infectabilty / 10
        self.duration = randint(7, 15)
        self.model = model

    def remove(self):
        self.model.grid._remove_agent(self.pos, self)
        self.model.schedule.remove(self)

    def add(self, row, column):
        self.model.schedule.add(self)
        self.model.grid.place_agent(self, (row, column))

    def step(self):
        if self.duration <= 0:
            self.remove()
            return

        self.duration -= 1
        self.check_contact()

    def check_contact(self):
        cellmates = self.model.grid.get_cell_list_contents([self.pos])
        if len(cellmates) > 1:
            other = self.random.choice(cellmates)
            if type(other) is Susceptible:
                if self.Infect(other):
                    print(f"Susceptible n° {other.unique_id} infettato!")
                    other.remove()
                    # rimuovo l'agent dallo scheduler del model
                    a = Infected(other.unique_id,  # creo il corrispondente agent Infected
                                 other.gender,
                                 other.age,
                                 other.ill,
                                 other.wear_mask,
                                 other.wear_gloves,
                                 other.model,
                                 1)
                    a.add()  # posiziono il nuovo agent nella griglia
            elif self.model.infect_again and type(other) is Recovered:
                if self.Infect(other):
                    print(f"Guarito n° {other.unique_id} infettato di nuovo!")
                    other.remove()
                    # rimuovo l'agent dallo scheduler del model
                    a = Infected(other.unique_id,  # creo il corrispondente agent Infected
                                 other.gender,
                                 other.age,
                                 other.ill,
                                 other.wear_mask,
                                 other.wear_gloves,
                                 other.model,
                                 1)
                    a.add()  # posiziono il nuovo agent nella griglia

    def Infect(self, agent):
        return random.random() < (agent.infectability * self.infectivity)
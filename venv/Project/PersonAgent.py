from mesa import Agent
from Project.Wall import Wall
from numpy import random
from random import randrange, randint
from Project.person_status import *
from Project.model_types import types
from Project.constants import *
from Project.navigator import Pathfinder
import math



class Person(Agent):
    def __init__(self,
                 id,
                 gender,
                 age,
                 bIll,
                 bWearMask,
                 bWearGloves,
                 model,
                 destination=[],
                 path=[]):
        super().__init__(id, model)
        self.unique_id      = id
        self.destination    = destination
        self.path           = path
        self.gender         = gender
        self.age            = age
        self.ill            = bIll
        self.wear_mask      = bWearMask
        self.wear_gloves    = bWearGloves
        self.destination    = destination
        self.infectability  = self.get_infectability()
        self.move_random    = 0

        self.status             = status['init']    # se init significa che l'agent deve essere inizializzato per intero
        self.out_grid_duration  = 0
        self.already_self_quarantine = False
        self.age_coeff          = ((math.floor(self.age / 10)) ** 2) / 100
        self.can_go_auto_quarantine = 0

        self.contacts = []

    def step(self):
        if self.model.type == types['open']:
            """in this way, agents choose only one time if go on self-quarantine, instead to valuate it at every step"""
            if self.model.schedule.steps == 0 and random.random() < self.model.self_quarantine_prob / 100:
                self.can_go_auto_quarantine = random.choice(range(1, 20)) * STEPSDAY
                print(f"{type(self).__name__} n° {self.unique_id} può andare in autoquarantena")

            if self.status == status['quarantine']:
                if self.model.break_quarantine_prob > 0 and self.model.schedule.steps % STEPSDAY == 0 and random.random() < self.model.break_quarantine_prob / 100:
                    #print(f"{type(self).__name__} n° {self.unique_id} non rispetta la quarantena!")
                    self.stop_quarantine()
                if self.out_grid_duration > 0 and self.model.schedule.steps % self.out_grid_duration == 0:
                    if not type(self).__name__ == "Infected":
                        self.stop_quarantine()
                    else:
                        self.out_grid_duration = self.model.quarantine_duration

            elif self.status == status['self_quarantine']:
                if self.model.schedule.steps % self.out_grid_duration == 0:
                    self.stop_self_quarantine()
            else:
                if self.model.self_quarantine_prob > 0:
                    if not self.model.repeat_self_quarantine:
                        if not self.already_self_quarantine and self.can_go_auto_quarantine > 0:
                            if self.model.schedule.steps > 0 and self.model.schedule.steps % self.can_go_auto_quarantine == 0:
                                self.start_self_quarantine()
                    else:
                        if random.random() < self.model.self_quarantine_prob / 1000:
                            self.start_self_quarantine()

        if self.model.type == types['close']:
            if any(self.pos == e.position for e in self.model.floor.exits):     # if agent is above any exit, remove it
                self.model.schedule.agents_out[type(self)][self.unique_id] = self
                self.remove()
                return

            if self.pos in self.destination:
                self.destination.remove(self.destination[0])
                self.move_random = random.choice(range(5, 10))

        if self.status == status['on_grid']:
            self.move()

    def move(self):
        if self.move_random <= 0:
            if len(self.destination) > 0:
                # path = Pathfinder(self.model).find_shortest_path(self.pos, self.destination[0])
                """find the next cell where to go, if exist"""
                path = Pathfinder(self.model).find_next_cell(self.pos, self.destination[0])
                if path:
                    self.path = path
                    self.model.grid.move_agent(self, path)
                    # self.model.grid.move_agent(self, self.path)
                return
        else:
            self.move_random -= 1

        adjacent_cells = self.model.grid.get_neighborhood(  # ritorna le celle adiacenti
            self.pos,
            moore=False,  # escludo le celle diagonali
            include_center=False
        )

        if self.model.social_distancing and random.random() < 1 - (self.model.break_social_distancing / 100):  # simulo la possibilità che un agent non rispetti il social distancing
            self.apply_social_distancing(adjacent_cells)
            return

        while True:
            new_position = self.random.choice(adjacent_cells)
            cell = self.model.grid.get_cell_list_contents(new_position)

            """controllo se la cella scelta è vuota, ed in caso sposto l'agent"""
            if self.model.grid.is_cell_empty(new_position):
                self.model.grid.move_agent(self, new_position)
                break
            if not type(cell[0]) is Wall:
                self.model.grid.move_agent(self, new_position)
                break

    def remove(self):
        if self.status == status['self_quarantine']:
            self.stop_self_quarantine()
        elif self.status == status['quarantine']:
            self.stop_quarantine()
        #elif self.status == status['hospitalized']:
        #    self.model.schedule.hospitalized_agents.remove(self)

        self.model.schedule.remove(self)

        if self.status == status['on_grid']:
            self.remove_from_grid()

    def remove_from_grid(self):
        self.model.grid.remove_agent(self)  # rimuovo l'agent  dalla griglia

    def add(self, pos=None):
        self.model.schedule.add(self)
        if self.status == status['init']:
            self.status = status['on_grid']
            self.add_to_grid(pos=pos)
            return

        if not self.status == status['on_grid']:
            self.add_to_grid(pos=pos)

    def add_to_grid(self, pos=None):
        """if agent has to be placed at entrance of floor"""
        if pos is not None:
            self.model.grid.place_agent(self, pos)
            return

        while True:
            x = self.model.random.randrange(self.model.grid.width)
            y = self.model.random.randrange(self.model.grid.height)
            cell = self.model.grid.get_cell_list_contents((x, y))

            """Controllo se la cella è vuota"""
            if self.model.grid.is_cell_empty((x, y)):
                self.model.grid.place_agent(self, (x, y))
                break
            """Controllo se la cella contiene un muro"""
            if not self.model.social_distancing:
                if not type(cell[0]) is Wall:
                    self.model.grid.place_agent(self, (x, y))
                    break

    def get_infectability(self):

        infectability = self.model.prob_be_infected / 100
        new_infectability = 0
        var_count = 0
        if self.wear_mask:
            var_count += 1
            new_infectability += self.model.prob_be_infected_wearing_mask / 100
        if self.wear_gloves:
            var_count += 1
            new_infectability += self.model.prob_be_infected_wearing_gloves / 100

        if new_infectability > 0:
            return new_infectability / var_count
        else:
            return infectability

        use_sanitizer = self.model.sanitizers and (random.random() < (self.model.prob_sanitizer_used / 100))
        if use_sanitizer:
            infectability = (infectability - 0.1) if infectability > 0 else 0.1
            #print(f"{type(self).__name__} n° {self.unique_id} ha usato il disinfettante - infettabilità: {infectability}")
        return infectability

    def apply_social_distancing(self, adjacent_cells):
        deadlock = True
        valid_cells = []
        """controllo se ogni cella contiene già un agent qualsiasi"""
        for cell in adjacent_cells:
            if self.model.grid.is_cell_empty(cell):
                deadlock = False
                valid_cells.append(cell)

        """se tutte le celle sono già occupate, sposto l'agent su una cella vuota randomicamente"""
        if deadlock:
            self.model.grid.move_to_empty(self)
        else:
            new_position = self.random.choice(valid_cells)
            self.model.grid.move_agent(self, new_position)

    def start_self_quarantine(self):
        if (self.status == status['on_grid'] or self.status == status['self_quarantine']):
            self.can_go_auto_quarantine = 0
            self.status = status['self_quarantine']
            self.model.schedule.self_quarantine_agents.append(self)
            if self.pos:
                self.remove_from_grid()
            self.out_grid_duration = self.model.self_quarantine_duration + random.choice(range(-5, 5)) * STEPSDAY
            self.already_self_quarantine = True
            #print(f"{type(self).__name__} n° {self.unique_id} è in auto-quarantena per {self.out_grid_duration} step")

    def stop_self_quarantine(self):
        #print(f"{type(self).__name__} n° {self.unique_id} uscito dall'auto-quarantena")
        self.status = status['on_grid']
        self.model.schedule.self_quarantine_agents.remove(self)
        self.add_to_grid()

    def start_quarantine(self):
        if not self.status == status['hospitalized'] and not self.status == status['quarantine']:
            if self.status == status['self_quarantine']:
                self.stop_self_quarantine()
            self.status = status['quarantine']
            self.model.schedule.quarantine_agents.append(self)
            if self.pos:
                self.remove_from_grid()
            self.out_grid_duration = self.model.quarantine_duration
            #print(f"{type(self).__name__} n° {self.unique_id} in quarantena per {self.out_grid_duration} step")
        else:
            pass
            #print(f"{type(self).__name__} n° {self.unique_id} - quarantena non applicabile - stato: {self.status}")

    def stop_quarantine(self):
        #print(f"{type(self).__name__} n° {self.unique_id} esce dalla quarantena")
        self.status = status['on_grid']
        self.model.schedule.quarantine_agents.remove(self)
        self.add_to_grid()

    def add_contact(self, agent_id):
        if len(self.contacts) == self.model.contacts_tracking_capacity:
            self.remove_last_contact()

        if agent_id in self.contacts:
            self.remove_contact(agent_id)
        if random.random() < self.model.contacts_tracking_precision / 100:
            self.contacts.append(agent_id)
        else:
            pass
            # print(f"{type(self).__name__} n° {self.unique_id}: contatto non tracciato...")

    def remove_last_contact(self):
        if not len(self.contacts) == 0:
            self.contacts.remove(self.contacts[-1])

    def remove_contact(self, agent_id):
        if agent_id in self.contacts:
            self.contacts.remove(agent_id)

    def get_first_contact(self):
        if len(self.contacts) > 0:
            return self.contacts[1]

    def check_contacts(self):
        # print(f"Contact tracking a partire da {type(self).__name__} n° {self.unique_id}...")
        # pprint(self.contacts, indent=2)

        checking_agents = self.contacts

        while len(self.contacts) > 0:
            agent_id = self.contacts[0]
            self.contacts.remove(agent_id)

            agent = self.model.schedule.get_agent_by_id(agent_id)
            if agent is not None:
                self.model.make_tampon(agent)

        self.contacts = checking_agents

from mesa import Model
from mesa.space import MultiGrid
from Project.Scheduler import COVID_Scheduler
from mesa.datacollection import DataCollector
from numpy import random
from random import randrange
import math
from Project.constants import *

from Project.person_status import status
from Project.model_types import types
from Project.Infected import Infected
from Project.Susceptible import Susceptible
from Project.Recovered import Recovered

from Project.Wall import Wall
from Project.FloorMap import FloorMap
from Project.navigator import Node


class CovidModel(Model):
    def __init__(
            self,
            model_type=types['none'],   # specify the model type
            num_suscetible=1,           # initial number of Susceptible
            num_infected=1,             # initial number of Infeceted
            prob_is_man=50,             # prob. agent is man
            prob_wear_mask=50,          # prob. agent is wearing mask
            prob_wear_gloves=50,        # prob. agent is wearing gloves
            prob_is_asyntomatic=50,     # prob. infected is asymptomatic
            prob_is_ill=20,             # prob. agent is ill
            prob_infect=1,              # prob. Infected to infecet a Susceptible
            prob_be_infected=1,         # prob. Susceptible to be infected
            prob_infect_wearing_mask=3,             # prob. to infect wearig mask
            prob_infect_wearing_gloves=3,           # prob. to infect wearing gloves
            prob_be_infected_wearing_mask=3,        # prob. to be infected wearing mask
            prob_be_infected_wearing_gloves=3,      # prob. to be infected wearing gloves
            prob_enter_someone=50,      # prob. someone enter
            prob_enter_infected=0,      # prob. enter an infected
            sanitizers=False,           # entrances are provided with sanitaziers
            prob_sanitizer_used=50,     # prob. agents use sanitaziers
            break_quarantine_prob=0,    # prob. that agents break quarantine
            quarantine_duration=0,      # quarantine duration
            self_quarantine_prob=0,     # prob. agents go self-quarantine
            self_quarantine_duration=0, # self-quarantine duration
            repeat_self_quarantine=False,       # agents can go self-quarantine more times
            social_distancing=False,            # agents mantain social distancing
            break_social_distancing=False,      # prob. agents break social distancing
            periodic_tampons=False,             # tampons are made periodically
            tampons_rate=0,                     # periodically tampons rate
            periodic_tampons_prob=0,            # prob. agents are subjected to periodic tampons
            symptoms_tampons=False,             # agents can make tampons if show symptoms
            infect_again=False,                 # Recovered can be infected again
            contacts_tracking=False,            # Contact tracking is active on each agent
            contacts_tracking_precision=0,      # prob. that a contact is missed
            contacts_tracking_capacity=0,       # num. of last contact saved for each agent
            air_recirculation=True,             # rooms are provided with air recirculation
            prob_be_infected_no_recirculation=0 # probability that agents are infected without air recirculation
    ):

        super().__init__()

        self.type                       = model_type
        self.num_agents                 = num_suscetible + num_infected
        self.num_suscetible             = num_suscetible
        self.num_infected               = num_infected
        self.prob_is_man                = prob_is_man
        self.prob_wear_mask             = prob_wear_mask
        self.prob_wear_gloves           = prob_wear_gloves
        self.prob_infect_wearing_mask           = prob_infect_wearing_mask
        self.prob_infect_wearing_gloves         = prob_infect_wearing_gloves
        self.prob_be_infected_wearing_mask      = prob_be_infected_wearing_mask
        self.prob_be_infected_wearing_gloves    = prob_be_infected_wearing_gloves
        self.prob_is_asyntomatic        = prob_is_asyntomatic
        self.prob_is_ill                = prob_is_ill
        self.prob_infect                = prob_infect
        self.prob_be_infected           = prob_be_infected
        self.prob_enter_someone         = prob_enter_someone
        self.prob_enter_infected        = prob_enter_infected
        self.sanitizers                 = sanitizers
        self.prob_sanitizer_used        = prob_sanitizer_used
        self.num_recovered              = 0
        self.break_quarantine_prob      = break_quarantine_prob
        self.quarantine_duration        = quarantine_duration * STEPSDAY
        self.self_quarantine_prob       = self_quarantine_prob
        self.self_quarantine_duration   = self_quarantine_duration * STEPSDAY
        self.repeat_self_quarantine     = repeat_self_quarantine
        self.periodic_tampons           = periodic_tampons
        self.tampons_rate               = tampons_rate * STEPSDAY
        print(self.tampons_rate)
        self.periodic_tampons_prob      = periodic_tampons_prob
        self.symptoms_tampons           = symptoms_tampons
        self.infect_again               = infect_again
        self.social_distancing          = social_distancing
        self.break_social_distancing    = break_social_distancing
        self.contacts_tracking              = contacts_tracking
        self.contacts_tracking_precision    = contacts_tracking_precision
        self.contacts_tracking_capacity     = contacts_tracking_capacity
        self.air_recirculation              = air_recirculation
        self.prob_be_infected_no_recirculation = prob_be_infected_no_recirculation

        self.schedule                   = COVID_Scheduler(self)
        self.running                    = True

        if self.type == types['open']:
            # Wall.Build_BotSxRoom(self)
            # Wall.Build_BotDxRoom(self)
            # Wall.Build_UpSxRoom(self)
            # Wall.Build_UpDxRoom(self)
            self.width = 50
            self.height = 50
            self.grid = MultiGrid(self.width, self.height, True)

            self.datacollector = DataCollector(
                {
                    "Day":                      lambda m: m.schedule.get_day(),
                    "Infected":                 lambda m: m.schedule.get_breed_count(Infected),
                    "Susceptible":              lambda m: m.schedule.get_breed_count(Susceptible),
                    "Recovered":                lambda m: m.schedule.get_breed_count(Recovered),
                    "Dead":                     lambda m: m.schedule.get_dead_count(),
                    "Self-quarantine":          lambda m: m.schedule.get_self_quarantine_count(),
                    "Total Tampons":            lambda m: m.schedule.get_tot_tampons_count(),
                    "Positive Tampons":         lambda m: m.schedule.get_positive_tampons_count(),
                    "Quarantine":               lambda m: m.schedule.get_quarantine_count(),
                    "Hospitalized":             lambda m: m.schedule.get_curr_hospitalized_agents(),
                    "Total Hospitlized":        lambda m: m.schedule.get_tot_hospitalized_agents(),
                    "Intensive_care":           lambda m: m.schedule.get_current_intensive_care_agents(),
                    "Total Intesive care":      lambda m: m.schedule.get_tot_intensive_care_agents(),
                    "Man count":                lambda m: m.schedule.get_man_count(),
                    "Woman count":              lambda m: m.schedule.get_woman_count(),
                    "Average age":              lambda m: m.schedule.get_avg_age()
                }
            )

            """create Susceptibles"""
            for i in range(1, self.num_suscetible + 1):
                gender      = "M" if random.random() < (self.prob_is_man / 100) else "F"
                if gender == 'M':
                    self.schedule.man_count += 1
                else:
                    self.schedule.women_count += 1
                age         = randrange(6, 91)
                bIll        = random.random() < self.prob_is_ill / 100 and random.random() < math.floor(age / 10) / 100
                bWearMask   = random.random() < self.prob_wear_mask / 100
                bWearGloves = random.random() < self.prob_wear_gloves / 100

                a = Susceptible(
                    i,
                    gender,
                    age,
                    bIll,
                    bWearMask,
                    bWearGloves,
                    self
                )
                a.add()

            """create Infected"""
            for i in range(self.num_suscetible + 1, self.num_suscetible + self.num_infected + 1):
                gender       = "M" if random.random() < (self.prob_is_man / 100) else "F"
                if gender == 'M':
                    self.schedule.man_count += 1
                else:
                    self.schedule.women_count += 1
                age         = randrange(6, 91)
                bIll = random.random() < self.prob_is_ill / 100 and random.random() < math.floor(age / 10) / 100
                bWearMask = random.random() < self.prob_wear_mask / 100
                bWearGloves = random.random() < self.prob_wear_gloves / 100

                a = Infected(
                    i,
                    gender,
                    age,
                    bIll,
                    bWearMask,
                    bWearGloves,
                    self,
                    1
                )
                a.add()

            self.schedule.calculate_avg_age()

        elif self.type == types['close']:

            self.width = 20
            self.height = 20
            self.grid = MultiGrid(self.width, self.height, True)
            self.datacollector = DataCollector(
                {
                    "Infected": lambda m: m.schedule.get_breed_count(Infected),
                    "Susceptible": lambda m: m.schedule.get_breed_count(Susceptible),
                    "Infected entered": lambda m: m.schedule.get_entered_agents_count(Infected),
                    "Susceptible entered": lambda m: m.schedule.get_entered_agents_count(Susceptible),
                    "Infected out": lambda m: m.schedule.get_agents_out_count(Infected),
                    "Susceptible out": lambda m: m.schedule.get_agents_out_count(Susceptible),
                    "Agents infected": lambda m: m.schedule.get_agents_infected(),
                    "Infection ratio": lambda m: m.schedule.get_infection_ratio()
                }
            )

            """create the floor structure reading the JSON file into the path declared"""
            self.floor = FloorMap("res/floor2.json", self)

    def step(self):
        if self.type == types['open']:
            self.schedule.calculate_day()
            if self.periodic_tampons and self.periodic_tampons_prob > 0:
                if self.schedule.steps % self.tampons_rate == 0 and self.schedule.steps > 1:
                    print("Avvio tamponi...")
                    self.tampon_randomly()
        elif self.type == types['close']:
            """if S.C. is active, check if the entrance choosen is empty"""
            entrance_choosen = random.choice(self.floor.entrances)
            if self.social_distancing:
                entrance_content = self.grid.get_cell_list_contents(entrance_choosen.position)
                entrance_busy = any([type(x) is Susceptible or type(x) is Infected for x in entrance_content])
                if not entrance_busy:
                    self.agent_random_entrance(entrance_choosen.position)
            else:
                self.agent_random_entrance(entrance_choosen.position)

            """if rooms are not provided with air recirculation, then the air will saturate"""
            if not self.air_recirculation:
                self.floor.saturate_rooms_air()

            for room in self.floor.rooms:
                room.update_current_agents()

        self.schedule.step()
        self.datacollector.collect(self)

    """randomly make tampon to certain number of agents that are actually on grid"""
    def tampon_randomly(self):
        valid_agents = [agent for agent in self.schedule.agents if agent not in self.schedule.hospitalized_agents
                        and agent not in self.schedule.quarantine_agents and not type(agent) is Wall]
        tampons_num = math.floor(len(valid_agents) * self.periodic_tampons_prob / 100)
        for i in range(tampons_num):
            agent = random.choice(valid_agents)
            valid_agents.remove(agent)
            self.make_tampon(agent)

    """make tampon to the agent passed as param and if it's positive, he goes quarantine"""
    def make_tampon(self, agent):

        self.schedule.tot_tampons += 1
        if not agent.status == status['quarantine'] and not agent.status == status['hospitalized']:
            if type(agent) is Infected and not agent.dead:
                if agent.unique_id not in self.schedule.pos_tampon_agents:
                    self.schedule.positive_tampons += 1
                    self.schedule.pos_tampon_agents.append(agent.unique_id)

                #print(f"Agent n° {agent.unique_id} positivo al tampone! Viene applicata la quarantena")
                agent.start_quarantine()
                if self.contacts_tracking and len(agent.contacts) > 0:
                    agent.check_contacts()

        elif agent.status == status['hospitalized']:
            if type(agent) is Infected and not agent.dead:
                if agent.unique_id not in self.schedule.pos_tampon_agents:
                    self.schedule.positive_tampons += 1
                    self.schedule.pos_tampon_agents.append(agent.unique_id)

                if self.contacts_tracking and len(agent.contacts) > 0:
                    agent.check_contacts()

    """method to manage agents entrance into close space. The entrance param is the entrance of the new agent"""
    def agent_random_entrance(self, entrance):
        if random.random() < self.prob_enter_someone / 100:
            gender = "M" if random.random() < (self.prob_is_man / 100) else "F"
            if gender == 'M':
                self.schedule.man_count += 1
            else:
                self.schedule.women_count += 1
            age = randrange(6, 91)
            bIll = random.random() < self.prob_is_ill / 100 and random.random() < math.floor(age / 10) / 100
            bWearMask = random.random() < self.prob_wear_mask / 100
            bWearGloves = random.random() < self.prob_wear_gloves / 100

            if random.random() < self.prob_enter_infected / 100:
                a = Infected(
                    self.schedule.next_id(),
                    gender,
                    age,
                    bIll,
                    bWearMask,
                    bWearGloves,
                    self,
                    1,
                    destination=self.assign_destinations()
                )
                #a.add(random.choice(self.floor.entrances).position)
                a.add(entrance)
                self.schedule.entered_agents[type(a)][a.unique_id] = a
            else:
                a = Susceptible(
                    self.schedule.next_id(),
                    gender,
                    age,
                    bIll,
                    bWearMask,
                    bWearGloves,
                    self,
                    destination=self.assign_destinations(),
                )
                # a.add(random.choice(self.floor.entrances).position)
                a.add(entrance)
                self.schedule.entered_agents[type(a)][a.unique_id] = a

    """before new agent enter into the floor, a list of rooms is assigned to him. At the end, a random exit is added to the list """
    def assign_destinations(self):
        """assign an index to each room, then pick one randomly. This is the first room to visit"""
        rooms = range(0, len(self.floor.rooms))
        selected_rooms = [random.choice(range(min(rooms), max(rooms)))]
        dest = []
        room_visited = random.choice(range(1, 6))
        for i in range(room_visited):
            """when choose next dest., pick a differente room from the last one"""
            values = [i for i in rooms if i != selected_rooms[-1]]
            r = random.choice(values)
            selected_rooms.append(r)

        """then convert each index to the corresponding room"""
        for idx in selected_rooms:
            dest.append(self.floor.rooms[idx].center)

        """at then end, add a randomly exit"""
        dest.append(random.choice(self.floor.exits).position)
        return dest

    """metodo di utility per visualizzare il funzionamento del BFS sulla grid"""
    def explore(self):
        tmp_list = []
        while len(self.Q) > 0:
            node = self.Q.pop(0)
            self.visited.append(node)
            adjacent_cells = self.get_adjacent_cells(node.position)
            for c in adjacent_cells:
                if not any(x.position == c for x in self.visited):
                    n = Node(self.schedule.next_id(Node), node, c, self)
                    self.schedule.add(n)
                    self.grid.place_agent(n, n.position)
                    tmp_list.append(n)

        self.Q = tmp_list

    def get_adjacent_cells(self, pos):
        y, x = pos
        grid = self.grid
        cells = []
        for y2, x2 in ((y, x + 1), (y + 1, x), (y, x - 1), (y - 1, x)):
            if 0 <= x2 < grid.width and 0 <= y2 < grid.height:
                type_cell = None
                if not grid.is_cell_empty((y2, x2)):
                    type_cell = grid.get_cell_list_contents((y2, x2))[0]

                if type(type_cell) is not Wall and type(type_cell) is not Node:
                    cells.insert(0, (y2, x2))

        return cells
from collections import defaultdict

from mesa.time import RandomActivation
from Project.constants import *

class COVID_Scheduler(RandomActivation):
    """
    A scheduler which activates each type of agent once per step, in random
    order, with the order reshuffled every step.

    This is equivalent to the NetLogo 'ask breed...' and is generally the
    default behavior for an ABM.

    Assumes that all agents have a step() method.
    """


    def __init__(self, model):
        super().__init__(model)
        self.agents_by_breed = defaultdict(dict)
        self.dead_agents                = []
        self.hospitalized_agents        = []    # lista di tutti gli ospedalizzati correnti
        self.hospitalized_agents_tot    = 0     # lista del totale di pazienti ospedalizzati
        self.self_quarantine_agents     = []    # lista di tutti gli agent in auto quarantena
        self.quarantine_agents          = []
        self.entered_agents             = defaultdict(dict)
        self.agents_out                 = defaultdict(dict)
        self.tot_tampons                = 0
        self.positive_tampons           = 0
        self.current_intensive_care     = []
        self.tot_intensive_care         = 0
        self.man_count                  = 0
        self.women_count                = 0
        self.day                        = 0
        self.avg_age                    = 0
        self.id_count                   = 1
        self.pos_tampon_agents          = []
        self.agents_infected            = 0

    def add(self, agent):
        """
        Add an Agent object to the schedule

        Args:
            agent: An Agent to be added to the schedule.
        """

        self._agents[agent.unique_id] = agent
        agent_class = type(agent)
        self.agents_by_breed[agent_class][agent.unique_id] = agent

    def remove(self, agent):
        """
        Remove all instances of a given agent from the schedule.
        """

        del self._agents[agent.unique_id]

        agent_class = type(agent)
        del self.agents_by_breed[agent_class][agent.unique_id]

    def step(self, by_breed=True):
        """
        Executes the step of each agent breed, one at a time, in random order.

        Args:
            by_breed: If True, run all agents of a single breed before running
                      the next one.
        """
        if by_breed:
            for agent_class in self.agents_by_breed:
                self.step_breed(agent_class)
            self.steps += 1
            self.time += 1
        else:
            super().step()

    def step_breed(self, breed):
        """
        Shuffle order and run all agents of a given breed.

        Args:
            breed: Class object of the breed to run.
        """
        agent_keys = list(self.agents_by_breed[breed].keys())
        self.model.random.shuffle(agent_keys)
        for agent_key in agent_keys:
            self.agents_by_breed[breed][agent_key].step()

    def get_breed_count(self, breed_class):
        """
        Returns the current number of agents of certain breed in the queue.
        """
        return len(self.agents_by_breed[breed_class].values())

    def get_dead_count(self):
        return len(self.dead_agents)

    def get_quarantine_count(self):
        return len(self.quarantine_agents)

    def get_self_quarantine_count(self):
        return len(self.self_quarantine_agents)

    def get_agent_by_pos(self, pos):
        for agent in self.agents:
            if agent.pos == pos:
                return agent

    def get_agent_by_id(self, id):
        for agent in self.agents:
            if agent.unique_id == id:
                return agent

        return None

    def next_id(self, breed=None):
        self.id_count += 1
        if breed is not None:
            return self.get_breed_count(breed) + 1
        else:
            return self.id_count

    def get_agents_out_count(self, breed_class):
        return len(self.agents_out[breed_class].values())

    def get_entered_agents_count(self, breed_class):
        return len(self.entered_agents[breed_class].values())

    def get_curr_hospitalized_agents(self):
        return len(self.hospitalized_agents)

    def get_tot_hospitalized_agents(self):
        return self.hospitalized_agents_tot

    def get_current_intensive_care_agents(self):
        return len(self.current_intensive_care)

    def get_tot_intensive_care_agents(self):
        return self.tot_intensive_care

    def calculate_day(self):
        if self.steps % STEPSDAY == 0:
            self.day += 1

    def get_day(self):
        return self.day

    def get_man_count(self):
        return self.man_count

    def get_woman_count(self):
        return self.women_count

    def calculate_avg_age(self):
        tot_age = 0
        for a in self.agents:
            tot_age += a.age

        self.avg_age = tot_age / len(self.agents)

    def get_avg_age(self):
        return self.avg_age

    def get_tot_tampons_count(self):
        return self.tot_tampons


    def get_positive_tampons_count(self):
        return self.positive_tampons

    def get_agents_infected(self):
        return self.agents_infected

    def get_infection_ratio(self):
        if self.steps > 0:
            return round((self.agents_infected / self.steps) * 100, 2)
        else:
            return "0.00"
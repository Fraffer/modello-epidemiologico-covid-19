from Project.Servers import *
from Project.COVID_Models import CovidModel
from mesa.visualization.ModularVisualization import ModularServer


value = input('Choose which model start:\n 1 --> COVID Open Space\n 2 --> COVID Work Place\n')

if value == '1':
    server = ModularServer(CovidModel,
                           [open_grid, Counters_openspace(), chart_openspace],
                           "COVID Model on Open Space",
                           model_params_openspace)
    server.port = 8521  # The default
    server.launch()
elif value == '2':
    server = ModularServer(CovidModel,
                           [close_grid, Counters_closespace(), chart_closespace],
                           "COVID Model on Open Space",
                           model_params_closespace)
    server.port = 8521  # The default
    server.launch()

else:
    print('Input non valido.')

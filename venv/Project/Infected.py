from Project.PersonAgent import Person
from Project.Susceptible import Susceptible
from Project.model_types import types
from numpy import random
from random import randrange, randint
import math
from Project.Recovered import Recovered
from Project.person_status import *
from Project.ExitAgent import Exit
from Project.EntryAgent import Entry
from Project.infection_stages import infection_stages
from Project.constants import *

class Infected(Person):
    def __init__(self,
                 id,
                 gender,
                 age,
                 bIll,
                 bWearMask,
                 bWearGloves,
                 model,
                 infection_status,
                 destination=[],
                 path=[]):
        super().__init__(id, gender, age, bIll, bWearMask, bWearGloves, model, destination=destination, path=path)

        self.infection_status   = infection_stages['asyntomatic']  # indica la gravità dell'infezione.
        #self.worsen_step_list   = self.Get_step_to_worse()
        #self.decay              = self.Get_decay_time()  # tempo di decadimento del vrius
        #self.healing            = self.decay  # tempo di guarigione rimanente (in continuo aggiornamento)

        self.step_to_heal       = random.choice(range(5, 8)) * STEPSDAY + random.choice(range(-10, 10))
        self.dead               = False
        self.infectivity        = self.get_infectivity()
        self.infection_coeff    = self.age_coeff + self.get_worse_coefficient()
        if self.infection_coeff >= 1:
            self.infection_coeff = 0.9
        self.death_prob = self.get_death_prob()
        self.infection_duration = 0

        print(f"Infected n° {self.unique_id} - "
              f"gender: {self.gender} - "
              f"age:{self.age} - "
              f"ill: {self.ill} - "
              f"mask: {self.wear_mask} - "
              f"gloves: {self.wear_gloves} - "
              f"intensity: {self.infection_status} - "
              #f"peggioramento: {self.worsen_step_list} - "
              #f"decadimento: {self.decay} - "
              f"stato: {self.infection_status} - "
              f"inf_coff: {self.infection_coeff} - "
              f"guarigione: {self.step_to_heal}")

    def step(self):
        if self.model.type == types['open']:
            if self.infection_status >= infection_stages['hospitalized']:
                if not self.status == status['hospitalized']:
                    self.hospitalization()

            if self.model.symptoms_tampons and self.status == status['on_grid'] and self.infection_status == infection_stages['symptoms'] and random.random() < self.infection_status / 100:
                #print(f"Agent n° {self.unique_id} esegue il tampone in quanto presenta sintomi...")
                self.model.make_tampon(self)

            if self.model.schedule.steps > 0 and self.model.schedule.steps % self.step_to_heal == 0:
                if self.infection_status == infection_stages['intensive_care']:
                    if random.random() < self.death_prob:
                        #print(f"{type(self).__name__} n° {self.unique_id} deceduto")
                        self.dead = True
                        self.model.schedule.dead_agents.append(self)
                        if self.infection_status == infection_stages['hospitalized']:
                            self.model.schedule.hospitalized_agents.remove(self)
                        elif self.infection_status == infection_stages['intensive_care']:
                            self.model.schedule.current_intensive_care.remove(self)
                        self.remove()
                        return
                    else:
                        self.remove()
                        self.Heal()
                        return
                elif random.random() < self.infection_coeff:
                    self.worsen_infection()
                else:
                    self.remove()
                    self.Heal()
                    return
        super().step()

    def move(self):
        super().move()
        cellmates = self.model.grid.get_cell_list_contents([self.pos])
        if len(cellmates) > 1:
            if self in cellmates:
                cellmates.remove(self)
            for agent in cellmates:
                if type(agent) is Exit or type(agent) is Entry:
                    continue
                #agent = self.random.choice(cellmates)
                
                if self.model.contacts_tracking:
                    self.add_contact(agent.unique_id)
                #print(f"Contatto tra Infetto n°{self.unique_id} e {type(agent).__name__} n° {agent.unique_id}")
                if type(agent) is Susceptible:
                    if self.infect(agent):
                        self.model.schedule.agents_infected += 1
                        #print(f"Susceptible n° {agent.unique_id} infettato!")
                        agent.remove()
                        # rimuovo l'agent dallo scheduler del model
                        a = Infected(agent.unique_id,  # creo il corrispondente agent Infected
                                     agent.gender,
                                     agent.age,
                                     agent.ill,
                                     agent.wear_mask,
                                     agent.wear_gloves,
                                     agent.model,
                                     1,
                                     destination=agent.destination,
                                     path=agent.path)
                        a.add(pos=agent.pos)  # posiziono il nuovo agent nella griglia
                elif type(agent) is Recovered and self.model.infect_again:
                    if self.infect(agent):
                        self.model.schedule.agents_infected += 1
                        #print(f"Guarito n° {agent.unique_id} infettato di nuovo!")
                        agent.remove()
                        # rimuovo l'agent dallo scheduler del model
                        a = Infected(agent.unique_id,  # creo il corrispondente agent Infected
                                     agent.gender,
                                     agent.age,
                                     agent.ill,
                                     agent.wear_mask,
                                     agent.wear_gloves,
                                     agent.model,
                                     1)
                        a.add()  # posiziono il nuovo agent nella griglia

    def get_infectivity(self):

        infectivity = self.model.prob_infect / 100
        new_infectivity = 0
        var_count = 0
        if self.wear_mask:
            var_count += 1
            new_infectivity += self.model.prob_infect_wearing_mask / 100
        if self.wear_gloves:
            var_count += 1
            new_infectivity += self.model.prob_infect_wearing_gloves / 100

        if new_infectivity > 0:
            return new_infectivity / var_count
        else:
            return infectivity

    """return the probability that an agent get worse"""
    def get_worse_coefficient(self):
        percentage = 0.0

        if self.infection_status == infection_stages['asyntomatic']:
            percentage = random.choice(range(40, 60)) / 100
            #print(f"Agent n° {self.unique_id} age: {self.age} ill: {self.ill} worse_perc: {percentage}")

        if self.infection_status == infection_stages['symptoms']:
            percentage = random.choice(range(12, 18)) / 100
            #print(f"Agent n° {self.unique_id} age: {self.age} ill: {self.ill} worse_perc: {percentage}")

        if self.infection_status == infection_stages['hospitalized']:
            percentage = random.choice(range(1, 2)) / 100
            #print(f"Agent n° {self.unique_id} age: {self.age} ill: {self.ill} worse_perc: {percentage}")

        if self.ill:
            disease_severity = random.choice(range(8, 13))
            percentage += (percentage * disease_severity / 100) if (percentage * disease_severity / 100) <= 0.9 else 0.9
            #print(f"Agent n° {self.unique_id} age: {self.age} ill: {self.ill} worse_perc: {percentage}")
        return percentage

    def get_death_prob(self):
        percentage = 0
        if math.floor(self.age / 10) == 0:
            percentage = 0.14
        if math.floor(self.age / 10) == 1:
            percentage = 0.0
        if math.floor(self.age / 10) == 2:
            percentage = 0.1
        if math.floor(self.age / 10) == 3:
            percentage = 0.32
        if math.floor(self.age / 10) == 4:
            percentage = 0.94
        if math.floor(self.age / 10) == 5:
            percentage = 2.77
        if math.floor(self.age / 10) == 6:
            percentage = 10.87
        if math.floor(self.age / 10) == 7:
            percentage = 26.69
        if math.floor(self.age / 10) == 8:
            percentage = 34.77
        if math.floor(self.age / 10) == 9:
            percentage = 33.39

        if self.ill:
            percentage += 10

        return percentage / 100

    def worsen_infection(self):
        self.step_to_heal = random.choice(range(2, 5)) * STEPSDAY + self.model.schedule.steps + random.choice(range(-10, 10))
        self.infection_status += 1
        self.infection_coeff = self.age_coeff + self.get_worse_coefficient()
        if self.infection_status == infection_stages['hospitalized']:
            self.model.schedule.hospitalized_agents.append(self)
            self.model.schedule.hospitalized_agents_tot += 1

        if self.infection_status == infection_stages['intensive_care']:
            self.model.schedule.hospitalized_agents.remove(self)
            self.model.schedule.current_intensive_care.append(self)
            self.model.schedule.tot_intensive_care += 1

        #print(f"{type(self).__name__} n° {self.unique_id} è peggiorato allo stato: {self.infection_status} - inf_coeff: {self.infection_coeff} - guarigione: {self.step_to_heal}")

    def infect(self, susceptible):
        #return random.random() < (susceptible.infectability * self.infectivity)
        return random.random() < self.infectivity * susceptible.infectability

    def Heal(self):
        #print(f"Guarito agente n. {self.unique_id}")
        if self.infection_status == infection_stages['hospitalized']:
            self.model.schedule.hospitalized_agents.remove(self)
        elif self.infection_status == infection_stages['intensive_care']:
            self.model.schedule.current_intensive_care.remove(self)

        if self.unique_id in self.model.schedule.pos_tampon_agents:
            self.model.schedule.pos_tampon_agents.remove(self.unique_id)
        a = Recovered(
            self.unique_id,
            self.gender,
            self.age,
            self.ill,
            self.wear_mask,
            self.wear_gloves,
            #self.status if not self.status == status['on_grid'] else status['init'],
            status['init'],
            self.out_grid_duration,
            self.already_self_quarantine,
            self.contacts,
            self.model
        )
        """if a.status == status['self_quarantine']:
            self.model.schedule.add(a)
            a.start_self_quarantine()
        elif self.status == status['quarantine']:
            self.model.schedule.add(a)
            a.start_quarantine()
        else:
            a.add()"""
        a.add()

    def hospitalization(self):
        if self.status == status['on_grid']:
            self.model.grid.remove_agent(self)
        elif self.status == status['self_quarantine']:
            self.model.schedule.self_quarantine_agents.remove(self)
        elif self.status == status['quarantine']:
            self.model.schedule.quarantine_agents.remove(self)

        self.status = status['hospitalized']
        self.model.make_tampon(self)
        #print(f"Infetto n° {self.unique_id} ricoverato!")

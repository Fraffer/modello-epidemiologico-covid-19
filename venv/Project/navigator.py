from mesa.agent import Agent
from Project.Wall import Wall
from Project.ExitAgent import Exit
from Project.EntryAgent import Entry
from numpy import random


class Pathfinder:
    def __init__(self, model):
        self.model = model

    def find_shortest_path(self, start, destination):
        class TreeNode:
            def __init__(self, parent, pos):
                self.parent = parent
                self.position = pos

        start_node = TreeNode(0, start)
        path = []
        Q = [start_node]
        visited = []
        """ init the list that will contain new childs"""
        tmp_list = []
        while len(Q) > 0:
            """extract each node from Q taking every time the first from left"""
            node = Q.pop(0)
            """check if the node is the destination"""
            if node.position == destination:
                """build backtrack path looping parent node"""
                while node.position != start:
                    path.append(node)
                    node = node.parent

                return path  # ritorno il percorso

            visited.append(node)
            """get all avaible and not visited adjacent cells"""
            adjacent_cells = self.get_unvisited_neighbors(node.position, visited)
            for c in adjacent_cells:
                """for each adj cell check if it's already in the childs list"""
                if not any(x.position == c for x in tmp_list):
                    child_node = TreeNode(node, c)
                    tmp_list.append(child_node)

            """when Q is empty, put all new childs into Q to explore the next branch"""
            if len(Q) == 0:
                if len(tmp_list) > 0:
                    Q = tmp_list
                    tmp_list = []
                else:
                    return False  # non esiste nessun percorso

    def find_next_cell(self, start, destination):
        class TreeNode:
            def __init__(self, parent, pos):
                self.parent = parent
                self.position = pos

        start_node = TreeNode(0, start)
        path = []
        Q = [start_node]
        visited = []
        """ init the list that will contain new childs"""
        tmp_list = []
        while len(Q) > 0:
            """extract each node from Q taking every time the first from left"""
            node = Q.pop(0)
            """check if the node is the destination"""
            if node.position == destination:
                """build backtrack path looping parent node"""
                while node.position != start:
                    path.append(node)
                    node = node.parent

                return path[-1].position  # return next cell where to move

            visited.append(node)
            """get all avaible and not visited adjacent cells"""
            adjacent_cells = self.get_unvisited_neighbors(node.position, visited, start)
            for c in adjacent_cells:
                """for each adj cell check if it's already in the childs list"""
                if not any(x.position == c for x in tmp_list):
                    child_node = TreeNode(node, c)
                    tmp_list.append(child_node)

            """when Q is empty, put all new childs into Q to explore the next breadth level"""
            if len(Q) == 0:
                if len(tmp_list) > 0:
                    Q = tmp_list
                    tmp_list = []
                else:
                    return start  # no path exists

    def get_unvisited_neighbors(self, pos, visited, start):
        y, x = pos
        grid = self.model.grid
        social_distancing = self.model.social_distancing
        cells = []
        for y2, x2 in ((y, x + 1), (y + 1, x), (y, x - 1), (y - 1, x)):
            if 0 <= x2 < grid.width and 0 <= y2 < grid.height:

                valid_cell = True
                if not grid.is_cell_empty((y2, x2)):
                    cell_content = grid.get_cell_list_contents((y2, x2))
                    for element in cell_content:
                        if type(element) is Wall:  # a prescindere, se nella cella c'è un muro, la scarto
                            valid_cell = False
                            """ 
                                if S.C. is active, check if agent is inside a room. If True, he can only exit, 
                                else he can only entry  
                            """
                        elif self.model.social_distancing and random.random() < 1 - self.model.break_social_distancing / 100:
                            if pos == start:  # se la cella coincide con la posizione attuale dell'agent (solo ciclo),
                                              # devo assicurarmi che nelle celle adiacenti non ci siano altri agent
                                if type(element) is not Exit and type(element) is not Entry:
                                    valid_cell = False
                            if self.model.floor.is_agent_any_room(pos): # se l'agent è in una stanza, potrà passare solo per l'uscita
                                if type(element) is Entry:
                                    valid_cell = False
                            else:
                                # se l'agent è all'esterno, potrà scegiere solo entrate, a meno che non si tratti di un'uscita dal piano
                                if type(element) is Exit and not any(element.position == x.position for x in self.model.floor.exits):
                                    valid_cell = False

                if valid_cell and not any(x.position == (y2, x2) for x in visited):
                    cells.insert(0, (y2, x2))
        return cells


class Node(Agent):
    def __init__(self, id, parent, pos, model, ):
        self.unique_id = id
        self.parent = parent
        self.position = pos
        self.model = model

    def step(self):
        if self.pos == self.model.floor.get_room_by_id('D')[0].center:
            return print("Trovato")
        adjacent_cells = self.get_adjacent_cells(self.position)
        print(f"{self.parent.position if self.parent is not None else 'Start'} <-- NODO {self.position}:")
        for cell in adjacent_cells:
            n = Node(self.model.schedule.next_id(type(self)), self, cell, self.model)
            self.model.schedule.add(n)
            self.model.grid.place_agent(n, n.position)
            print(f"child: {n.position}")
